package com.lenyiova;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Honeywell {
    public static int doMagicWithTickets(List<Integer> tickets) {
        if (tickets.isEmpty() || tickets.size() > Math.pow(10, 5)) throw new IllegalArgumentException();
        if (tickets.size() == 1) return 1;      // Works for both cases -> Length of sub must be 1; Number of subs must be 1;

        tickets.sort(Comparator.naturalOrder());
        System.out.println(tickets);            // Control print of sorted tickets

        List<List<Integer>> subsequences = new ArrayList<>();

        int firstPoint = 0;
        for (int i = 0; i < tickets.size() - 1; i++) {
            if (tickets.get(i + 1) - tickets.get(i) > 1 || i == tickets.size() - 2) {
                List<Integer> s = tickets.subList(firstPoint, i + 1);

                if (i == tickets.size() - 2) {
                    List<Integer> t;
                    if (tickets.get(tickets.size() - 1) - tickets.get(tickets.size() - 2) <= 1) {
                        t = new ArrayList<>(s);
                        t.add(tickets.get(tickets.size() - 1));
//                        s.add(tickets.get(tickets.size() - 1));         // java.lang.UnsupportedOperationException
//                        subsequences.add(s);
                    } else {
                        t = new ArrayList<>();
                        t.add(tickets.get(tickets.size() - 1));
                        subsequences.add(s);
                    }
                    subsequences.add(t);
                } else {
                    subsequences.add(s);
                }
                firstPoint = i + 1;
            }
        }

        List<List<Integer>> sortedSubsequences = sortListsBySize(subsequences);

        // Print all subsequences.
        for (List<Integer> list : sortedSubsequences) {
            System.out.println(list);
        }

        // Print size of the longest subsequence.
        System.out.println(sortedSubsequences.get(sortedSubsequences.size() - 1).size());
        return sortedSubsequences.get(sortedSubsequences.size() - 1).size();

//        System.out.println("There are " + subsequences.size() + " subsequences");
//        return subsequences.size();
    }

    private static List<List<Integer>> sortListsBySize(List<List<Integer>> list) {
        boolean checkChanges = true;

        while (checkChanges) {
            checkChanges = false;
            for (int i = 0; i < list.size() - 1; i++) {
                List<Integer> a = list.get(i);
                List<Integer> b = list.get(i + 1);
                if (a.size() > b.size()) {
                    List<Integer> helper = new ArrayList<>(a);
                    list.set(i, b);
                    list.set(i + 1, helper);
                    checkChanges = true;
                }
            }
        }

        return list;
    }
}
