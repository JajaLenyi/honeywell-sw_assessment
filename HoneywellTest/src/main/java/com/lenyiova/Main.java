package com.lenyiova;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<Integer> numbers = Arrays.asList(-6, -5, -4, -1, 2, 3, 4, 4, 13, 14, 16, 17, 17, 21, 22, 22, 23, 45);
        Honeywell.doMagicWithTickets(numbers);

    }
}
