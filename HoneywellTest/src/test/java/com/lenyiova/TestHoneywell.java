package com.lenyiova;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class TestHoneywell {

    @Test
    public void testEmptyListInput() {
        List<Integer> emptyList = Collections.EMPTY_LIST;

        Assertions.assertThrows(IllegalArgumentException.class, new Executable() {
            @Override
            public void execute() {
                Honeywell.doMagicWithTickets(emptyList);
            }
        });
    }

    @Test
    public void testTooLargeInput() {
        int[] array = new int[(int) Math.pow(10, 5) + 1];
        List<Integer> largeInput = new ArrayList<>();                        // new ArrayList<>(array);   nevim proc to nejde
        for (int i : array) largeInput.add(i);

        Assertions.assertThrows(IllegalArgumentException.class, new Executable() {
            @Override
            public void execute() {
                Honeywell.doMagicWithTickets(largeInput);
            }
        });
    }

    // ------- (1) Return number of subsequences ----------

    @Test
    public void testOneItemList1() {
        List<Integer> oneItemList = Collections.singletonList(4);

        int result = Honeywell.doMagicWithTickets(oneItemList);

        Assertions.assertEquals(1, result);
    }

    @Test
    public void testTwoItemList1_firstNumberEqualsSecondNumber() {
        List<Integer> twoItemsList = Arrays.asList(1, 1);

        int result = Honeywell.doMagicWithTickets(twoItemsList);

        Assertions.assertEquals(1, result);
    }

    @Test
    public void testTwoItemList1_firstNumberSmallerThanSecondNumber() {
        List<Integer> twoItemsList = Arrays.asList(1, 3);

        int result = Honeywell.doMagicWithTickets(twoItemsList);

        Assertions.assertEquals(2, result);
    }

    @Test
    public void testLongerList1() {
        List<Integer> list = Arrays.asList(3, 2, 4, 13, 4);

        int result = Honeywell.doMagicWithTickets(list);

        Assertions.assertEquals(2, result);
    }

    // ------- (2) Return length of the longest subsequence --------

    @Test
    public void testOneItemList2() {
        List<Integer> oneItemList = Collections.singletonList(4);

        int result = Honeywell.doMagicWithTickets(oneItemList);

        Assertions.assertEquals(1, result);
    }

    @Test
    public void testTwoItemList2_firstNumberEqualsSecondNumber() {
        List<Integer> twoItemsList = Arrays.asList(1, 1);

        int result = Honeywell.doMagicWithTickets(twoItemsList);

        Assertions.assertEquals(2, result);
    }

    @Test
    public void testTwoItemList2_firstNumberSmallerThanSecondNumber() {
        List<Integer> twoItemsList = Arrays.asList(1, 3);

        int result = Honeywell.doMagicWithTickets(twoItemsList);

        Assertions.assertEquals(1, result);
    }

    @Test
    public void testLongerList2() {
        List<Integer> list = Arrays.asList(3, 2, 4, 13, 4);

        int result = Honeywell.doMagicWithTickets(list);

        Assertions.assertEquals(4, result);
    }
}
